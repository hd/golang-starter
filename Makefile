# mainly calls scripts from /scripts
include .env
MAKEFLAGS += --silent

.PHONY: help build test
## help: shows this help message
help: Makefile
	@echo
	@echo " Choose a command run in "$(PROJECTNAME)":"
	@echo
	@sed -n 's/^##//p' $< | column -t -s ':' |  sed -e 's/^/ /'
	@echo

## vars: prints build variables
vars:
	@echo "PROJECTNAME : ${PROJECTNAME}"
	@echo "MODULENAME  : ${MODULENAME}"
	@echo "GOBASE      : ${GOBASE}"
	@echo "GOBIN       : ${GOBIN}"
	@echo "GOLIST      : ${GOLIST}"
	@echo "STDERR      : ${STDERR}"
	@echo "PID         : ${PID}"

# -- ALL Checks -----------------------------------------------

## all: build from scratch
all: clean inspect test rebuild
build: rebuild
rebuild: clean go-build

PLATFORMS := linux/amd64 linux/arm64 darwin/arm64

.PHONY: $(PLATFORMS)
$(PLATFORMS):
	GOOS=$(word 1, $(subst /, ,$@)) \
	GOARCH=$(word 2, $(subst /, ,$@)) \
	sh -c 'echo " .. Building for $$GOOS-$$GOARCH..."; go build -trimpath -ldflags "-w -s -extldflags ''" -o "$(GOBIN)/$(PROJECTNAME)-$$GOOS-$$GOARCH" $(MAINGO)'

.PHONY: go-build
go-build:
	@go get
	@go mod verify
	@echo " .. Building binaries..."
	$(MAKE) $(PLATFORMS)

clean:
	@echo " .. Cleaning build cache..."
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) go clean
	@echo " .. Cleaning prebuilt binaries..."
	@rm -rf $(GOBIN)

# -- TEST ------------------------------------------------

## test: Runs unit tests
test:
	@mkdir -p coverage
	@go test -race -v -coverpkg=./... -cover -coverprofile=coverage/covereage.out ./...
	@go tool cover -html=coverage/covereage.out -o coverage/covereage.html
	@go tool cover -func=coverage/covereage.out | grep total:

## inspect: Runs source code inspections
inspect:
	@go fmt $(GOLIST)
	@go vet $(GOLIST)

# -- MODULE/DEPENDENCIES ------------------------------------------------

## tidy: tidies dependencies
tidy:
	@echo "tidying modules..."
	@go mod tidy

dependency-update:
	@echo "updating dependencies..."
	@go get -u
	@go mod tidy

## update: updates and tidies dependencies
update: tidy dependency-update
