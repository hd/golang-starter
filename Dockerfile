FROM golang:1.22-bookworm as builder
ARG PLATFORM="linux/amd64"
ARG MAINGO="main.go"

WORKDIR /build
COPY . .
RUN \
    export GOOS=$(echo $PLATFORM | cut -d/ -f1) ;\
    export GOARCH=$(echo $PLATFORM | cut -d/ -f2) ;\
    echo "GOOS: ${GOOS}; GOARCH: ${GOARCH}" ;\
    go build -trimpath -ldflags "-w -s -extldflags ''" -o "main" ${MAINGO}

# -----------------------------------
FROM scratch

COPY --from=builder /build/main /main

ENTRYPOINT [ "/main" ]
